Demo Project for the CE base board (HW rev. 3.00) populated with NUCLEO-F042K6
==

![Picture of CE Board](CeBoard_NUCLEO-F042K6.jpg)

Features
--
- two different LED pattern, toggled by pressing button T2
- LED pattern speed adjusted by ambient light (photo resistor)
- plays a tiny melody by pressing button T1
- UART output 115200 8N1 via USB to a terminal prog

Flashing the Board (Firmware Update)
--
- connect USB to PC/Laptop
- a new mass storage device will pop-up
- drag the firmware (.bin) onto the mass storage device
- done. The board will reboot with the new firmware

Further Reading
--
- http://stm32.bauernoeppel.de
- https://www.st.com/en/evaluation-tools/nucleo-f042k6.html
