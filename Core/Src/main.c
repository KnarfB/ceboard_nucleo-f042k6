/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "adc.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <errno.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
int _write(int file, char *ptr, int len)
{
  HAL_StatusTypeDef status = HAL_UART_Transmit(&huart2, (uint8_t*)ptr, (uint16_t)len, HAL_MAX_DELAY);
  if(status==HAL_OK)
    return len;
  else {
    errno = EIO;
    return -1;
  }
}

// could be const, but that caused warnings in HAL_GPIO_WritePin
GPIO_TypeDef *led_port[8] = {
  LED0_GPIO_Port,
  LED1_GPIO_Port,
  LED2_GPIO_Port,
  LED3_GPIO_Port,
  LED4_GPIO_Port,
  LED5_GPIO_Port,
  LED6_GPIO_Port,
  LED7_GPIO_Port
};

const uint16_t led_pin[8] = {
  LED0_Pin,
  LED1_Pin,
  LED2_Pin,
  LED3_Pin,
  LED4_Pin,
  LED5_Pin,
  LED6_Pin,
  LED7_Pin
};

const char notes[][4] = {
  "C4",
  "C#4",
  "D4",
  "D#4",
  "E4",
  "F4",
  "F#4",
  "G4",
  "G#4",
  "A4",
  "A#4",
  "H4", // B4
  
  "C5",
  "C#5",
  "D5",
  "D#5",
  "E5",
  "F5",
  "F#5",
  "G5",
  "G#5",
  "A5",
  "A#5",
  "H5", // B5
};

#define TIM_FREQ 1000000

int periods[24];

void init_periods() {
  for(int i=0; i<sizeof(notes)/sizeof(notes[0]); ++i) {
    periods[i] = (int)(TIM_FREQ/(261.63*pow(2.0, i/12.0)));
  }
}

void play_note(char note[]) {
  for(int i=0; i<sizeof(notes)/sizeof(notes[0]); ++i) {
    if(strcmp(notes[i], note)==0) {
      static int last_period;
      int period = periods[i];
      if(period!=last_period) {
        __HAL_TIM_SET_COUNTER(&htim14, 0);
        __HAL_TIM_SET_AUTORELOAD(&htim14, period);
        __HAL_TIM_SET_COMPARE(&htim14, TIM_CHANNEL_1, period/2);
        HAL_TIM_GenerateEvent(&htim14, TIM_EVENTSOURCE_UPDATE);
        last_period = period;
      }
      printf("%s\n", note);
      HAL_Delay(200);
      return;
    }
  }
  printf("don't know how to play note '%s'\n", note);
}

void play(char melody[]) {
  char str[256]; // create a writable copy for strtok
  strncpy(str, melody, sizeof(str));
  //TODO: avoid strtok
  char *note = strtok(str, " ");
  HAL_TIM_PWM_Start(&htim14, TIM_CHANNEL_1);
  while(note!=NULL) {
    play_note(note);
    note = strtok(NULL, " ");
  }
  HAL_TIM_PWM_Stop(&htim14, TIM_CHANNEL_1);
}

void display(int x) {
  for(int i=0; i<8; ++i) {
    if(x & (1<<i)) 
      HAL_GPIO_WritePin(led_port[i], led_pin[i], GPIO_PIN_SET);
    else
      HAL_GPIO_WritePin(led_port[i], led_pin[i], GPIO_PIN_RESET);
  }
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART2_UART_Init();
  MX_ADC_Init();
  MX_TIM14_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  printf("Hello from Computer Engineering @ HTW Berlin\n");
  printf("for more info, see https://gitlab.com/KnarfB/ceboard_nucleo-f042k6\n");

  init_periods();
  int pattern = 0;
  int i = 0;
  while (1)
  {
    switch(pattern) {
      case 0:
        HAL_GPIO_TogglePin(led_port[i], led_pin[i]);
        i = (i+1) % 8;
        break;
      default:
        i = (i+1) % 256;
        display(i);
    }
    
    HAL_ADC_Start(&hadc);
    HAL_ADC_PollForConversion(&hadc, 100);
    uint32_t adcval = HAL_ADC_GetValue(&hadc);
    // linear interpolation
    int x0 = 0;
    int x1 = 4095;
    int y0 = 500;
    int y1 =  50;
    int xp = adcval;
    int yp = y0 + ((y1-y0)*(xp-x0))/(x1-x0);
    HAL_Delay(yp);

    if(HAL_GPIO_ReadPin(BUTTON1_GPIO_Port, BUTTON1_Pin)==GPIO_PIN_RESET) {
      printf("button 1 pressed\n");
      play("E5 D#5 E5 D#5 E5 H4 D5 C5 A4 A4 A4 C4 E4 A4 H4 H4 H4 E4 G#4 H4 C5 C5 C5"); // fur Elise 
    }
  
    if(HAL_GPIO_ReadPin(BUTTON2_GPIO_Port, BUTTON2_Pin)==GPIO_PIN_RESET) {
      pattern = (pattern+1) % 2;
      printf("button 2 pressed, switching to LED pattern %d\n", pattern);
      i = 0;
      display(i);
    }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI14|RCC_OSCILLATORTYPE_HSI48;
  RCC_OscInitStruct.HSI48State = RCC_HSI48_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI48;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_1) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
